@extends('account.layouts.default')

@section('account.content')
    <div class="card">
        <div class="card-body">
            <form action="{{ route('account.password.store') }}" method="POST">
                @csrf

                <div class="form-row">
                    <div class="col mb-3">
                        <label for="password_current">Current password</label>
                        <input type="password" class="form-control{{ $errors->has('password_current') ? ' is-invalid' : '' }}" id="password_current" name="password_current">
                        @if($errors->has('password_current'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('password_current') }}</strong>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="form-row">
                    <div class="col mb-3">
                        <label for="password_current">New password</label>
                        <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" name="password">
                        @if($errors->has('password'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="form-row">
                    <div class="col mb-3">
                        <label for="password_confirmation">Password confirmation</label>
                        <input type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" id="password_confirmation" name="password_confirmation">
                        @if($errors->has('password_confirmation'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </div>
                        @endif
                    </div>
                </div>

                <button class="btn btn-primary">Change password</button>
            </form>
        </div>
    </div>
@endsection
