@extends('account.layouts.default')

@section('account.content')
    <div class="card">
        <div class="card-body">
            <form action="{{ route('account.profile.store') }}" method="POST">
                @csrf

                <div class="form-row">
                    <div class="col mb-3">
                        <label for="name">Name</label>
                        <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" name="name" value="{{ old('name', auth()->user()->name)}}">
                        @if($errors->has('name'))
                            <div class="invalid-feedback">
                                Please provide a valid name.
                            </div>
                        @endif
                    </div>
                </div>

                <div class="form-row">
                    <div class="col mb-3">
                        <label for="email">Email</label>
                        <input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="name" name="email" value="{{ old('email', auth()->user()->email)}}">
                        @if($errors->has('email'))
                            <div class="invalid-feedback">
                                Please provide a valid email.
                            </div>
                        @endif
                    </div>
                </div>

                <button class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>
@endsection
